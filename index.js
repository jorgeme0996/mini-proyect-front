window.onload = function () {
    const userId = localStorage.getItem('userId')

    console.log(userId)
    if(userId) {
        window.location.href = "http://localhost:5500/tareas/tareas.html";
    } else {
        localStorage.clear()
    }
}

function handleSubmint(event) {
    event.preventDefault()
    const inputFields = document.querySelectorAll(".userInput");
    const data = {}

    for(let inputField of inputFields) {
        console.log(inputField.id, inputField.value);
        data[inputField.id] = inputField.value
    }

    const edad = calcularEdad(data.cumpleaños)
    console.log(edad);

    crearUsuario(data, edad)
}

function calcularEdad(fechaCumple) {
    var result = 0;
    const año = new Date().getFullYear()
    const añoDeCumple = new Date(fechaCumple).getFullYear()
    result = año - añoDeCumple;
    console.log(añoDeCumple, año);
    return result
}

function crearUsuario(data, edad) {
    console.log(data, edad);
    fetch('http://localhost:8080/user', {
        method: 'POST',
        body: JSON.stringify({
            nombre: data.nombre,
            apellido: data.apellido,
            //edad: edad
            edad
        }),
        headers: {"Content-type": "application/json; charset=UTF-8"}
    })
    .then(responce => responce.json())
    .then(data => {
        console.log(data);
        localStorage.setItem('userId', data.id);
        window.location.href = "http://localhost:5500/tareas/tareas.html";
    })
}