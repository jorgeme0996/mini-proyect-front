var USER;

window.onload = function () {
    const userId = localStorage.getItem('userId')

    console.log(userId)

    if(!userId) {
        window.location.href = "http://localhost:5500/";
    } else {
        irPorUsuario(userId)
    }
}

function irPorUsuario(userId) {
    fetch(`http://localhost:8080/user/${userId}`)
    .then(responce => {
        console.log(responce)
        if(responce.status !== 200){
            //window.location.href = "http://localhost:5500/";
        }
        return responce.json()
    })
    .then(data => {
        console.log(data)
        if(data.ok === false) {
            //window.location.href = "http://localhost:5500/";
        } else {
            USER = data.user
            llenarLaPantallaDelUsuario(USER)    
        }
    })
    .catch(error => {
        console.log(error)
        window.location.href = "http://localhost:5500/";
    })
}

function llenarLaPantallaDelUsuario(user) {
    
    document.getElementById('titulo').innerHTML = ` ${user.nombre} ${user.apellido}`

    const contTareas = document.getElementById('cont-tareas');

    let listaDeTareas = '';

    for(let [i, tarea] of user.tareas.entries()) {
        listaDeTareas +=`
            <div class="tarea">
                <p>${i + 1}. ${tarea.nombre}</p>
                <div class="cont-buttons-tarea">
                    <button type="button" class="btn btn-outline-warning" id="${tarea._id}" onclick="editTarea(event)">Editar</button>
                    <button type="button" class="btn btn-outline-danger" id="${tarea._id}" onclick="deleteTarea(event)">Eliminar</button>
                </div>
            </div>
        `
    }

    contTareas.innerHTML = listaDeTareas
}

function editTarea(event) {
    const tareaId = event.target.id
    fetch(`http://localhost:8080/tarea/${tareaId}`)
    .then(responce => responce.json())
    .then(data => {
        console.log(data);
        mostarModal(data.tarea)
    }).catch(error => {
        console.log(error)
    })
}

function mostarModal(tarea) {
    const body = document.querySelector('body');

    body.innerHTML += `
        <div id="modal">
            <div id="cont-form">
                <form onsubmit="handleEditarTarea(event)" id="${tarea._id}">
                    <input class="form-control me-2" type="text" value="${tarea.nombre}" id="tareaAct">
                    <button class="btn btn-outline-danger" onclick="closeModal()">Cancelar</button>
                    <button class="btn btn-outline-success" type="submit">Editar</button>
                </form>
            </div>
        </div>
    `
}

function closeModal() {
    document.getElementById('modal').remove()
}

function handleEditarTarea(event) {
    event.preventDefault()
    const tareaActualizada = document.getElementById('tareaAct').value
    console.log(tareaActualizada);
    const tareaId = event.target.id
    console.log(tareaId);
    fetch(`http://localhost:8080/actualizar/tarea/${tareaId}`, {
        method: 'PUT',
        body: JSON.stringify({
            nombre: tareaActualizada
        }),
        headers: {"Content-type": "application/json; charset=UTF-8"}
    })
    .then(responce => responce.json())
    .then(data => {
        console.log(data);
        closeModal()
        irPorUsuario(USER._id)
    }).catch(error => {
        console.log(error)
    })
}

function deleteTarea(event) {
    const tareaId = event.target.id
    fetch(`http://localhost:8080/eliminar/tarea/${tareaId}`, {
        method: 'DELETE'
    })
    .then(responce => responce.json())
    .then(data => {
        console.log(data);
        irPorUsuario(USER._id)
    }).catch(error => {
        console.log(error)
    })
}

function crearTarea(event) {
    event.preventDefault()
    const userId = localStorage.getItem('userId')

    const nombreTarea = document.getElementById('nombreTarea').value

    if(nombreTarea) {
        console.log(nombreTarea)
        fetch(`http://localhost:8080/tarea/user/${userId}`, {
            method: 'POST',
            body: JSON.stringify({
                nombre: nombreTarea
            }),
            headers: {"Content-type": "application/json; charset=UTF-8"}
        })
        .then(responce => responce.json())
        .then(data => {
            console.log(data);
            document.getElementById('nombreTarea').value = ""
            irPorUsuario(userId)
        }).catch(error => {
            console.log(error)
        })
    }
}